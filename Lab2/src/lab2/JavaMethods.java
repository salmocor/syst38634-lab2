package lab2;

public class JavaMethods {
	// option 1
	public static int smallestInt(int a, int b, int c) {
		return a < b ? a : (b < c ? b : c);
	}

	// option 2
	public static int smallestInt2(int nums[]) {
		int lowest = nums[0];
		for (int i : nums) {
			lowest = i < lowest ? i : lowest;
		}
		return lowest;
	}

	public static int mean(int nums[]) {
		int avg = 0;
		for (int i : nums) {
			avg += i;
		}
		return avg / nums.length;
	}

	public static String middleCharacters(String input) {
		return input.substring(input.length() / 2 + input.length() % 2 - 1, input.length() / 2 + input.length() % 2 - 1 + 1 + (1 - input.length() % 2));
	}
	
	public static int vowelCount(String input) {
		return (int)input.chars().filter(ch -> ch == 'i' || ch == 'e' || ch == 'a' ||  ch == 'o' || ch == 'u').count();
	}
	
	public static char charAt(String input, int pos) {
		return input.charAt(pos);
	}
	
	public static int pointCount(String input) {
		return input.codePointCount(0, input.length());
	}
}
