package lab2;

public class Main {

	public static void main(String[] args) {
		// Test 1
		int test1[] = { 25, 37, 29 };
		System.out.println("smallestInt2: " + JavaMethods.smallestInt2(test1));

		// Test 2
		int test2[] = { 25, 45, 65 };
		System.out.println("mean: " + JavaMethods.mean(test2));

		// Test 3
		String test3 = "350";
		System.out.println("middleCharacters: " + JavaMethods.middleCharacters(test3));

		// Test 4
		String test4 = "w3resource";
		System.out.println("vowelCount: " + JavaMethods.vowelCount(test4));

		// Test 5
		String test5 = "Java Exercises!";
		int test5int = 10;
		System.out.println("charAt: " + JavaMethods.charAt(test5, test5int));

		// Test 6
		String test6 = "w3rsource.com";
		System.out.println("pointCount: " + JavaMethods.pointCount(test6));
	}

}
